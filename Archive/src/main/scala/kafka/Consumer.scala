package kafka

import java.util.Properties

import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer

import scala.collection.JavaConverters._

object Consumer extends App {

  private def createConsumer(port: Int) = {
    val props = new Properties()
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, s"localhost:$port")
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getCanonicalName)
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getCanonicalName)
    props.put(ConsumerConfig.GROUP_ID_CONFIG, s"consumer")
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false")

    val consumer = new KafkaConsumer[String, String](props)
    consumer.subscribe(List("demo").asJava)
    consumer
  }

  val consumer1 = createConsumer(9092)
//  val consumer2 = createConsumer(9093)

  while (true) {
    consumer1.poll(1000).asScala.foreach(record => println(s"--- Received message from 9092: $record"))
    consumer1.commitSync()
//    consumer2.poll(1000).asScala.foreach(record => println(s"+++ Received message from 9093: $record"))
//    consumer2.commitSync()
  }

}
