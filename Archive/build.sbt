import sbt.Resolver

resolvers ++= Seq(
  "Maven central" at "http://central.maven.org/maven2/",
  "ovp" at "https://nexus.shared.int.ovp.bskyb.com/content/repositories/releases/",
  "Nexus Sky snapshots" at "https://nexus.shared.int.ovp.bskyb.com/content/repositories/snapshots/",
  "Sky Releases" at "https://nexus.api.bskyb.com/nexus/content/repositories/sky-releases/"
)

name := "kafkafailover"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies += "com.bskyb.ovp.nova" %% "activity-producer" % "1.5.2"