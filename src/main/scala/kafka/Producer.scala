package kafka

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer

object Producer extends App {
  val topic = "failover"

  val props = new Properties()
//  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "DEVML0222845:9092")
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "10.190.159.8:9092")
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)

  val producer = new KafkaProducer[String, String](props)
  producer.send(new ProducerRecord[String, String](topic, "name", "hello"))
  producer.close()
}