package kafka


import com.bskyb.ovp.activitycapture.producer.ActivityProducer.KafkaMetrics
import com.bskyb.ovp.activitycapture.producer.failover._
import com.bskyb.ovp.activitycapture.producer.guaranteed.AtLeastOnceDeliveryActivityProducer
import com.bskyb.ovp.activitycapture.producer.{ActivityProducerConfiguration, KafkaActivityProducer, ProducerMessage, ProducerMetadata}

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.language.postfixOps

object AutoFailover extends App {
  val primaryConfiguration = ActivityProducerConfiguration("10.190.169.110:9092", maxBlockTimeout = 1 second, name = "primary")
  val secondaryConfiguration = ActivityProducerConfiguration("10.190.159.8:9092", maxBlockTimeout = 1 second, name = "secondary")

  private val topic = "failover"
  val kafkaActivityProducer = new KafkaActivityProducer(primaryConfiguration)

  val atLeastOnceDeliveryActivityProducer = AtLeastOnceDeliveryActivityProducer(kafkaActivityProducer)

  // Use metrics to determine when to fail over, this example fails over as soon as the record-error-rate goes above zero.
  val shouldFailOver: KafkaMetrics => Boolean = { metrics =>
    metrics.asScala.toMap.filterKeys(_.name() == "record-error-rate")
      .filterKeys(_.group() == "producer-metrics")
      .values.map(_.value())
      .exists(_ > 0.0)
  }

  // Determine when to fall back to the primary cluster, after having failed over. This example always falls
  // back when the fallback check duration expires, but more sophisticated logic can be provided.
  val shouldFallback = () => true

  val failoverConfiguration = FailoverConfiguration(
    List(primaryConfiguration, secondaryConfiguration),
    shouldFailOver,
    shouldFallback,
    atLeastOnceDeliveryActivityProducer,
    5 seconds,                            // Frequency of failover check
    5 seconds,                            // Frequency of fallback check
    AutomaticFailoverStrategy             // Failover strategy - automatic or manual
  )

  val failoverControl = FailoverControl(failoverConfiguration)(kafkaActivityProducer.actorSystem)
  failoverControl.start()

//  Iterator.from(0).foreach { index =>
  0 to 200  foreach { index =>
      val messageContent =
        s"""{"id":"$index"}""".stripMargin

      val producerMetadata = ProducerMetadata(
        originatingSystem = "activity-producer-sample",
        activityType = "sample",
        activityTimestamp = new java.util.Date,
        providerTerritory = "GB",
        provider = "Sky",
        householdId = Some("householdId-" + index)
      )

      atLeastOnceDeliveryActivityProducer.send(ProducerMessage(topic, messageContent, producerMetadata))

      Thread.sleep(1000)
    }

  Thread.sleep(60000)
}